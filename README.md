RisoTechOJT
========================
> [DEV]For Riso Tech OJT

## Technology Stack 
- SpringMVC : Webアプリケーションを簡単に作るための機能を提供します。
- MyBatis : MyBatis はカスタム SQL、ストアドプロシージャ、高度なマッピング処理に対応した優れた永続化フレームワークです。
- Bootstrap : BootstrapはウェブサイトやWebアプリケーションを作成するフロントエンドWebアプリケーションフレームワークである。
- Mysql : MySQL（マイ・エスキューエル）とは世界中の多くの企業が使用しているデータベース管理システムです。
- Jquery : jQuery（ジェイクエリー）は、ウェブブラウザ用のJavaScriptコードをより容易に記述できるようにするために設計されたJavaScriptライブラリである。
- Maven : Maven はプロジェクト情報の一元管理、統一したビルドプロセスの提供、依存ライブラリの管理などの機能により、 開発者にかかるプロジェクト管理の負担を軽減します。


## Dependency
> Java v1.8
