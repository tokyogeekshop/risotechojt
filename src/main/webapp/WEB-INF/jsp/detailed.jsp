<%@page import="org.tgs.risotechojt.model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>detailed page</title>
<script type="text/javascript">
	function changeBirthReadonly() {
		var checkBox1 = document.getElementById('checkBox1').checked;
		var birth = document.getElementById('birth');
		true == checkBox1 ? birth.removeAttribute("readonly") : birth
				.setAttribute("readonly", "readonly");
	}

	function changeEmailReadonly() {
		var checkBox2 = document.getElementById('checkBox2').checked;
		var email = document.getElementById('email');
		true == checkBox2 ? email.removeAttribute("readonly") : email
				.setAttribute("readonly", "readonly");
	}

	function changePasswordReadonly() {
		var checkBox3 = document.getElementById('checkBox3').checked;
		var password = document.getElementById('password');
		true == checkBox3 ? password.removeAttribute("readonly") : password
				.setAttribute("readonly", "readonly");
	}
</script>



</head>
<body>

	<div align="center">
		<form id="" method="post" class="" commandName="userupdate">
			<input type="text" name="id" readonly="readonly"
				value="${requestScope.userMesssage.id}" />
			<table border="2">
				<tr>
					<td>姓名</td>
					<td><input type="text" name="userName" readonly="readonly"
						id="userNameInput" placeholder="User name"
						value="${requestScope.userMesssage.userName}" /></td>
				</tr>
				<tr>
					<td>出生日期</td>

					<td><input type="text" name="dateOfBirth" id="birth"
						placeholder="birth"
						value="<fmt:formatDate type="date" 
							value='${requestScope.userMesssage.dateOfBirth}' pattern="yyyy/MM/dd" />"
						readonly="readonly"> <input type="checkbox" id="checkBox1"
						onclick="changeBirthReadonly();"></td>
				</tr>
				<tr>
					<td>邮箱</td>
					<td><input type="email" name="emailAddress" id="email"
						placeholder="email"
						value=${requestScope.userMesssage.emailAddress
							}
						readonly="readonly" /><input type="checkbox" id="checkBox2"
						onclick="changeEmailReadonly();"></td>
				</tr>
				<tr>
					<td>密码</td>
					<td><input type="text" name="password" id="password"
						readonly="readonly" placeholder="password"
						value=${requestScope.userMesssage.password } /><input
						type="checkbox" id="checkBox3" onclick="changePasswordReadonly();"></td>
				</tr>
			</table>
			<button>提交</button>
		</form>

		<table border="2">
			<thead>
				<tr>
					<th scope="col">时间</th>
					<th scope="col">出勤</th>
					<th scope="col">退勤</th>
					<th scope="col">状况</th>
					<th scope="col">工时</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${requestScope.workMessage}" var="time">
					<tr>
						<td>${time.date}</td>
						<td><fmt:formatDate type="date" value='${time.workStartAt}'
								pattern="HH:mm:ss" /></td>
						<td><fmt:formatDate type="date" value='${time.workEndAt}'
								pattern="HH:mm:ss" /></td>
						<td>${time.workDivision}</td>
						<td>${time.workTime}</td>
					</tr>
				</c:forEach>
			</tbody>
			<tr>
				<th>合计工时</th>
				<th></th>
				<th></th>
				<th></th>
				<th>${sumTime}</th>
			</tr>
		</table>
	</div>
</body>
</html>


