<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.tgs.risotechojt.model.User"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>社員検索</title>
</head>
<body>
	<form method="post" class="" commandName="userSearch">

		<input type="text" placeholder="社員名前を入力" name="userName" />

		<button>検索</button>

		<table border="2">
			<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">姓名</th>
					<th scope="col">メールアドレス</th>
					<th scope="col">誕生日</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${requestScope.usersearch}" var="usersearch">
					<tr>
						<td>${usersearch.id}</td>
						<td><a href="detailed.html?userId=${usersearch.id}">
								${usersearch.userName}</a></td>
						<td>${usersearch.emailAddress}</td>
						<td><fmt:formatDate type="date"
								value='${usersearch.dateOfBirth}' pattern="yyyy/MM/dd" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form>
	<hr>
	<table class="table table-borderless" border="2">
		<thead>
			<tr>
				<th scope="col">ID</th>
				<th scope="col">姓名</th>
				<th scope="col">メールアドレス</th>
				<th scope="col">誕生日</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.alluser}" var="user">
				<tr>
					<td>${user.id}</td>
					<td><a href="detailed.html?userId=${user.id}">
							${user.userName}</a></td>
					<td>${user.emailAddress}</td>
					<td><fmt:formatDate type="date" value='${user.dateOfBirth}'
							pattern="yyyy/MM/dd" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>
