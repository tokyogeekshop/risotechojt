<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>deviceTable</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/themes/icon.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">

		function formatDeviceType(val,row){
			return val.typeName;
		}
		function formatDeviceStatus(val,row){
			if(val === 0){
				return '未貸し出し'
			}else{
				return '貸出済み'
			}
		}

		function searchDevice(){
			$("#dg").datagrid('load',{
				"name":$("#s_name").val()
			});
		}

		function deleteDevice(){
			var selectedRows=$("#dg").datagrid("getSelections");
			if(selectedRows.length==0){
				 $.messager.alert("システム提示","削除するデータを選択してください！");
				 return;
			 }
			 var strIds=[];
			 for(var i=0;i<selectedRows.length;i++){
				 strIds.push(selectedRows[i].id);
			 }
			 var ids=strIds.join(",");
			 $.messager.confirm("システム提示","確かに<font color=red>"+selectedRows.length+"</font>つのデータを削除しますか？",function(r){
					if(r){
						$.post("${pageContext.request.contextPath}/device/delete.do",{ids:ids},function(result){
							if(result.success){
								 $.messager.alert("システム提示","データの削除に成功しました！");
								 $("#dg").datagrid("reload");
							}else{
								$.messager.alert("システム提示","データ削除失敗！");
							}
						},"json");
					}
			});
		}

		function openDeviceModifyTab(){
			var selectedRows=$("#dg").datagrid("getSelections");
			if(selectedRows.length!=1){
			 $.messager.alert("システム提示","修正する設備を選んでください！");
			 return;
			}
			var row=selectedRows[0];
			window.location.href="${pageContext.request.contextPath}/deviceType/modify.do?id="+row.id;
		}

	</script>
</head>
<body>
<table id="dg" title="デバイス一覧" class="easyui-datagrid"
	   fitColumns="true" pagination="true" rownumbers="true"
	   url="${pageContext.request.contextPath}/device/list.do" fit="true" toolbar="#tb">
	<thead>
	<tr>
		<th field="cb" checkbox="true" align="center"></th>
		<th field="id" width="20" align="center">デバイスID</th>
		<th field="userId" width="20" align="center">社員ID</th>
		<th field="name" width="200" align="center" >デバイス名</th>
		<th field="time" width="50" align="center">期限</th>
		<th field="deviceType" width="50" align="center" formatter="formatDeviceType">分類</th>
		<th field="status" width="50" align="center"  formatter="formatDeviceStatus">回収区分</th>
	</tr>
	</thead>
</table>
<div id="tb">
	<div>
		<a href="javascript:openDeviceModifyTab()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修正</a>
		<a href="javascript:deleteDevice()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">削除</a>
	</div>
	<div>
		&nbsp;名：&nbsp;<input type="text" id="s_name" size="20" onkeydown="if(event.keyCode==13) searchDevice()"/>
		<a href="javascript:searchDevice()" class="easyui-linkbutton" iconCls="icon-search" plain="true">捜索</a>
	</div>
</div>


</body>
</html>