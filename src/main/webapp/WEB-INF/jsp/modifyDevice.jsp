<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 2020/2/21
  Time: 13:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>modify device</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/themes/icon.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript">



        function submitData(){
            var name=$("#name").val();
            var deviceTypeId=$("#deviceTypeId").combobox("getValue");
            var statusId=$("#status").combobox("getValue");
            var detail=$("#detail").val();

            if(name==null || name==''){
                alert("请输入设备名称！");
            }else if(deviceTypeId==null || deviceTypeId==''){
                alert("请选择设备类型！");
            }else if(detail==null || detail==''){
                alert("请输入设备详情！");
            }else{
                $.post("${pageContext.request.contextPath}/device/save.do",{'id':'${param.id}','name':name,'status':statusId,'deviceType.id':deviceTypeId,'detail':detail},function(result){
                    if(result.success){
                        alert("设备修改成功！");
                    }else{
                        alert("设备修改失败！");
                    }
                },"json");
            }
        }
    </script>

</head>
<body style="margin: 10px">

<div id="p" class="easyui-panel" title="デバイス詳細" style="padding: 10px">
    <table cellspacing="20px">
        <tr>
            <td width="80px">デバイス名：</td>
            <td><input type="text" id="name" name="name" style="width: 400px;"/></td>
        </tr>
        <tr>
            <td>分類：</td>
            <td>
                <select class="easyui-combobox" style="width: 154px" id="deviceTypeId" name="deviceType.id" editable="false" panelHeight="auto" >
                    <option value="">設備タイプを選択してください...</option>
                    <c:forEach var="deviceType" items="${requestScope.deviceTypeCountList}">
                        <option value="${deviceType.id }">${deviceType.typeName }</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>回収区分：</td>
            <td>
                <select class="easyui-combobox" style="width: 154px" id="status" name="status" editable="false" panelHeight="auto" >
                    <option value="">回収区分をお選びください...</option>
                    <option value="0">未貸し出し</option>
                    <option value="1">貸出済み</option>

                </select>
            </td>
        </tr>

        <tr>
            <td>デバイス詳細：</td>
            <td><input type="text" id="detail" name="detail" style="width: 400px;"/>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a href="javascript:submitData()" class="easyui-linkbutton" data-options="iconCls:'icon-submit'">发布设备</a>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">


    $(document).ready(function(){
        console.log("66666")
        $.ajax({
            url: "${pageContext.request.contextPath}/device/findById.do",
            type:"post",
            data:{"id":"${param.id}"},
            success:function(result){
                $("#name").val(result.name);
                $("#detail").val(result.detail);
                $("#deviceTypeId").combobox("setValue",result.deviceType.id);
                $("#status").combobox("setValue",+(result.status));
            }

        })

    })



</script>

</body>
</html>
