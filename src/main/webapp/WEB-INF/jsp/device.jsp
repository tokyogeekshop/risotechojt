<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>device</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/themes/icon.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">



		function submitData(){
			var name=$("#name").val();
			var deviceTypeId=$("#deviceTypeId").combobox("getValue");
			var statusId=$("#status").combobox("getValue");
			var detail=$("#detail").val();

			if(name==null || name==''){
				alert("設備名を入力してください！");
			}else if(deviceTypeId==null || deviceTypeId==''){
				alert("設備タイプを選択してください！");
			}else if(detail==null || detail==''){
				alert("設備の詳細を入力してください！");
			}else{
				$.post("${pageContext.request.contextPath}/device/save.do",{'name':name,'status':statusId,'deviceType.id':deviceTypeId,'detail':detail},function(result){
					if(result.success){
						resetValue();
						window.location.href="${pageContext.request.contextPath}/device/home.do"
					}else{
						alert("デバイス登録失敗！");
					}
				},"json");
			}
		}

		// reset
		function resetValue(){
			$("#name").val("");
			$("#deviceTypeId").combobox("setValue","");
			$("#status").combobox("setValue","");
			$("#detail").val("");
		}

	</script>
<style>

</style>
</head>
<body style="margin: 10px">


<div id="p" class="easyui-panel" title="デバイス登録" style="padding: 10px">
	<table cellspacing="20px">
		<tr>
			<td width="80px">デバイス名：</td>
			<td><input type="text" id="name" name="name" style="width: 400px;"/></td>
		</tr>
		<tr>
			<td>分類：</td>
			<td>
				<select class="easyui-combobox" style="width: 154px" id="deviceTypeId" name="deviceType.id" editable="false" panelHeight="auto" >
					<option value="">設備タイプを選択してください...</option>
					<c:forEach var="deviceType" items="${requestScope.deviceTypeCountList}">
						<option value="${deviceType.id }">${deviceType.typeName }</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>回収区分：</td>
			<td>
				<select class="easyui-combobox" style="width: 154px" id="status" name="status" editable="false" panelHeight="auto" >
					<option value="">回収区分をお選びください...</option>
					<option value=0>未貸し出し</option>
					<option value=1>貸出済み</option>

				</select>
			</td>
		</tr>

		<tr>
			<td>デバイス詳細：</td>
			<td><input type="text" id="detail" name="detail" style="width: 400px;"/>&nbsp;</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<a href="javascript:submitData()" class="easyui-linkbutton" data-options="iconCls:'icon-submit'">デバイス登録</a>
			</td>
		</tr>
	</table>
</div>



	<script type="text/javascript">
	

		
	
	
	

	</script>
</body>
</html>