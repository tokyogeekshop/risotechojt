<!DOCTYPE>
<html>
<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<link href="assets/css/bootstrap-united.css" rel="stylesheet" />
<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />
<style>
body {
	height: 100%;
	width: 100%;
	margin: 0;
	background: url(assets/img/books.png);
	background-size: cover;
	background-repeat: no-repeat;
	display: compact;
}
</style>
</head>
<body>
	<div class="navbar navbar-default">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
		</div>

		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<form class="navbar-form navbar-right">
				<input type="text" class="form-control" placeholder="Search">
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="index.jsp">ホーム</a></li>
				<li><a href="signup.html">サインアップ</a></li>
				<li><a href="login.html">サインイン</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">サーポト<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">開発者に問い合わせ</a></li>
						<li class="divider"></li>
						<li><a href="#">ほか対処</a></li>
						<li><a href="${pageContext.request.contextPath}/deviceType/toDevice.html">デバイス登録</a></li>
						<li><a href="${pageContext.request.contextPath}/device/home.do">デバイス リスト</a></li>
						<li><a href="${pageContext.request.contextPath}/deviceType/home.html">デバイスタイプリスト</a></li>
						<li><a href="userInfo.html">检索</a></li>
						<li><a href="attence.html">出勤</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.nav-collapse -->
	</div>
	<div class="container">
		<div class="jumbotron">
			<div>
				<h1>RISO TECH勤怠システムへよこそ！</h1>
				<p>毎日勤怠記録を良い習慣に、これを使って以下の問題を解決！</p>
				<p>・ミスが怖い</p>
				<p>・紙からデータ管理に変えると余計に時間がかかりそう</p>
				<p>・月末や締め日は残業しないと作業が終わらない</p>
			</div>

			<a class="btn btn-primary" href="signup.html">サインアップ » </a> <a
				class="btn btn-primary" href="login.html">サインイン » </a>
		</div>

		<div></div>
	</div>
	<script src="jquery-1.8.3.js">
		
	</script>

	<script src="bootstrap/js/bootstrap.js">
		
	</script>

</body>
</html>