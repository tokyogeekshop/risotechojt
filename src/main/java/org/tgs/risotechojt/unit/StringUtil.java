package org.tgs.risotechojt.unit;

public class StringUtil {
    public static String formatLike(String str){
        if(isNotEmpty(str)){
            return "%"+str+"%";
        }
        return null;
    }
    /**
     * 判断字符串是否为空
     */
    public static boolean isNotEmpty(String str){
        if(str !=null&& !"".equals(str.trim())){
            return true;
        }
        return false;
    }
}
