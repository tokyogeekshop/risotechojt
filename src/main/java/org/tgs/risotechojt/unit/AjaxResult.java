package org.tgs.risotechojt.unit;

import java.util.List;

import org.tgs.risotechojt.model.Device;
import org.tgs.risotechojt.model.User;

public class AjaxResult {
	private String massage;
	private Boolean iSuccess;
	
	private Object resultObject;
	private Object Data;
	
	private List<Device> devices ;
	private Device device;
	
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public List<Device> getDevices() {
		return devices;
	}
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}
	public String getMassage() {
		return massage;
	}
	public void setMassage(String massage) {
		this.massage = massage;
	}
	public Boolean getiSuccess() {
		return iSuccess;
	}
	public void setiSuccess(Boolean iSuccess) {
		this.iSuccess = iSuccess;
	}
	public Object getResultObject() {
		return resultObject;
	}
	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}
	public Object getData() {
		return Data;
	}
	public void setData(Object data) {
		Data = data;
	}
	
	
}
