package org.tgs.risotechojt.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.tgs.risotechojt.model.Device;
import org.tgs.risotechojt.model.DeviceType;
import org.tgs.risotechojt.model.DeviceVO;
import org.tgs.risotechojt.model.PageBean;
import org.tgs.risotechojt.service.DeviceService;
import org.tgs.risotechojt.unit.AjaxResult;
import org.tgs.risotechojt.unit.ResponseUtil;
import org.tgs.risotechojt.unit.StringUtil;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping({"/device"})
public class DeviceController {

	@Autowired
	private DeviceService deviceService;
	@RequestMapping({"/home"})
	public String home() {
		return "deviceTable";
	}

	/**
	 * save device
	 * 
	 * @return
	 */
	@RequestMapping({"/save"})
	public String save(Device device, HttpServletResponse response) throws IOException {
		int resultTotal = 0;
		if(device.getId()==null){
			resultTotal = deviceService.add(device);
		}else{
			resultTotal = deviceService.update(device);
		}
		JSONObject result = new JSONObject();
		if(resultTotal>0){
			result.put("success",Boolean.valueOf(true));
		}else{
			result.put("success",Boolean.valueOf(false));
		}
		ResponseUtil.write(response,result);
		return null;
	}
	/**
	 * device list
	 */
	@RequestMapping({"/list"})
//	@ResponseBody
	public String list(@RequestParam(value="page", required=false)String page,@RequestParam(value="rows",required=false)String rows,Device device,HttpServletResponse response) throws IOException {
		PageBean pageBean=new PageBean(Integer.parseInt(page),Integer.parseInt(rows));
//		DeviceVO vo = new DeviceVO();
//		if(null == pageBean){
//			return vo;
//		}
//		vo.setStart(pageBean.getStart());
//		vo.setSize(pageBean.getPageSize());
//		vo.setTotal(deviceService.getTotal());
//		vo.setStart(pageBean.getStart());
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("start",pageBean.getStart());
		map.put("size",pageBean.getPageSize());
		map.put("name", StringUtil.formatLike(device.getName()));
		List<Device> list = deviceService.list(map);
		Long total = deviceService.getTotal(map);
		JSONObject result = new JSONObject();
		JSONArray array = JSONArray.parseArray(JSON.toJSONString(list));
		result.put("rows",array);
		result.put("total",total);
		ResponseUtil.write(response,result);
		return null;
	}
	/**
	 *  to device.jsp
	 *
	 * @return
	 */
	@RequestMapping("/modify")
	public String modify() {
		return "modifyDevice";

	}
	/**
	 * key primary find device
	 */
    @RequestMapping(value="/findById", method=RequestMethod.POST)
	@ResponseBody
	public Device findById(@RequestParam("id")String id,HttpServletResponse response) throws IOException {
    	Device device = deviceService.findById(Integer.parseInt(id));
    	return device;
	}
	/**
	 * delete device
	 */
	@RequestMapping({"/delete"})
	public String delete(@RequestParam("ids")String ids, HttpServletResponse response) throws IOException {
		String[] idsStr =ids.split(",");
		for (int i = 0; i < idsStr.length; i++) {
			deviceService.delete(Integer.valueOf(idsStr[i]));
		}
		JSONObject result = new JSONObject();
		result.put("success",Boolean.valueOf(true));
		ResponseUtil.write(response,result);
		return null;
	}
}
