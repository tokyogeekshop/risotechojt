package org.tgs.risotechojt.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.tgs.risotechojt.model.User;
import org.tgs.risotechojt.model.UserLogin;
import org.tgs.risotechojt.model.WorkRecord;
import org.tgs.risotechojt.service.UserService;
import org.tgs.risotechojt.service.WorkService;

@Controller
@SessionAttributes("user")
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private WorkService WorkService;

  @RequestMapping(value = "/signup", method = RequestMethod.GET)
  public String signup(Model model) {
    User user = new User();
    model.addAttribute("user", user);
    return "signup";
  }

  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  public String signup(@ModelAttribute("user") User user, Model model) {
    if (userService.getUserByUserName(user.getUserName())) {
      model.addAttribute("message", "ユーザーID重複しました！");
      return "signup";
    } else {
      userService.insertUser(user);
      model.addAttribute("message", "新しいユーザーとして登録成功しました！");
      return "redirect:login.html";
    }
  }

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String login(Model model) {
    UserLogin userLogin = new UserLogin();
    model.addAttribute("userLogin", userLogin);
    return "login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(@ModelAttribute("userLogin") UserLogin userLogin) {
    boolean found = userService.getUserByLogin(userLogin.getUserName(), userLogin.getPassword());
    if (found) {
      return "success";
    } else {
      return "failure";
    }
  }

  @RequestMapping(value = "/detailed", method = RequestMethod.GET)
  public String detailed(ModelMap map, HttpServletRequest request, HttpServletResponse response) {

    long id = Long.parseLong(request.getParameter("userId"));
    // 个人信息
    User userMesssage = userService.getMesssage(id);
    map.addAttribute("userMesssage", userMesssage);
    // 工时列表
    List<WorkRecord> workMessage = WorkService.getWorkTime(id);
    map.put("workMessage", workMessage);
    // 全部工时
    int sumTime = 0;
    for (int i = 0; i < workMessage.size(); i++) {
      String sum = workMessage.get(i).getWorkTime();
      sumTime += Integer.parseInt(sum);
    }
    request.setAttribute("sumTime", sumTime);
    return "detailed";
  }

  @RequestMapping(value = "/detailed", method = RequestMethod.POST)
  public String detailed(@ModelAttribute("userupdate") User user) {
    userService.updateUser(user);
    return "success";
  }

  @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
  public String userall(ModelMap map) {
    List<User> alluser = userService.getalluser();
    map.put("alluser", alluser);
    return "userInfo";
  }

  @RequestMapping(value = "/userInfo", method = RequestMethod.POST)
  public String usersearch(@ModelAttribute("userSearch") User user, ModelMap map) {
    List<User> usersearch = userService.userSearch(user);
    map.put("usersearch", usersearch);
    return "userInfo";

  }

}
