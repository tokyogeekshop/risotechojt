package org.tgs.risotechojt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.tgs.risotechojt.model.WorkRecord;
import org.tgs.risotechojt.service.WorkService;

@Controller
@SessionAttributes("WorkRecord")
public class WorkController {

	@Autowired
	private WorkService WorkService;

	@RequestMapping(value = "/attence", method = RequestMethod.GET)
	public String work() {
		return "attence";
	}

	@RequestMapping(value = "/attence", method = RequestMethod.POST)
	public String workin(@ModelAttribute("workupdate") WorkRecord workRecord, Model model, HttpServletRequest request,
			HttpServletResponse response) {
//判断点击的按钮
		if ("clockin".equals(request.getParameter("submit"))) {
//over为已完成

			if ("over".equals(WorkService.Judgment(workRecord))) {
				model.addAttribute("message", "已完成出勤，不可修改！");
			} else if ("f1".equals(WorkService.Judgment(workRecord))) {
				model.addAttribute("message", "出勤时间已存在");
			} else {
				WorkService.updateStartTimeAndDivision(workRecord);
				model.addAttribute("message", "出勤登录成功");
			}
		}

		if ("clockout".equals(request.getParameter("submit"))) {

			if ("over".equals(WorkService.Judgment(workRecord))) {
				model.addAttribute("message", "已完成出勤，不可修改！");
			} else if ("f1".equals(WorkService.Judgment(workRecord))) {
				WorkService.updateEndTimeAndFlag(workRecord);
				model.addAttribute("message", "退勤成功");
			} else {
				model.addAttribute("message", "没找到出勤记录 ");
			}
		}

		if ("vacation".equals(request.getParameter("submit"))) {
			if ("over".equals(WorkService.Judgment(workRecord))) {
				model.addAttribute("message", "已完成出勤，不可修改！");
			} else if ("f1".equals(WorkService.Judgment(workRecord))) {
				model.addAttribute("message", " 出勤时间存在，全休失败 ");
			} else {
				WorkService.updateDivisionAndFlag(workRecord);
				model.addAttribute("message", "全休成功 ");
			}
		}
		return "attence";
	}

}
