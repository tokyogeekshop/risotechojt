package org.tgs.risotechojt.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.tgs.risotechojt.model.DeviceType;
import org.tgs.risotechojt.model.PageBean;
import org.tgs.risotechojt.service.DeviceTypeService;
import org.tgs.risotechojt.unit.ResponseUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * deviceType
 */
@Controller
@RequestMapping({"/deviceType"})
public class DeviceTypeController {
    @Resource
    private DeviceTypeService deviceTypeService;
    @RequestMapping({"/list"})
    public String list(@RequestParam(value="page", required=false)String page, @RequestParam(value="rows",required=false)String rows, HttpServletResponse response) throws IOException {
        PageBean pageBean = new PageBean(Integer.parseInt(page),Integer.parseInt(rows));
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("start",pageBean.getStart());
        map.put("size",pageBean.getPageSize());
        List<DeviceType> deviceTypeList = deviceTypeService.list(map);
        Long total = deviceTypeService.getTotal(map);
        JSONObject result = new JSONObject();
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(deviceTypeList));
        result.put("rows",array);
        result.put("total", total);
        ResponseUtil.write(response,result);

         return null;
    }
    @RequestMapping({"/save"})
    public String save(DeviceType deviceType, HttpServletResponse response) throws IOException {
        int resultTotal = 0;
        if(deviceType.getId() == null){
            resultTotal = deviceTypeService.add(deviceType).intValue();

        }else{
            resultTotal = deviceTypeService.update(deviceType).intValue();
        }
        JSONObject result = new JSONObject();
        if(resultTotal>0){
            result.put("success",Boolean.valueOf(true));
        }else{
            result.put("success",Boolean.valueOf(false));
        }
        ResponseUtil.write(response,result);
        return null;
    }
    @RequestMapping({"/delete"})
    public String delete(@RequestParam("ids")String ids, HttpServletResponse response) throws IOException {
        String[] idsStr = ids.split(",");
        for (int i = 0; i < idsStr.length; i++) {
            deviceTypeService.delete(Integer.valueOf(idsStr[i]));
        }
        JSONObject result = new JSONObject();
        result.put("success",Boolean.valueOf(true));
        ResponseUtil.write(response,result);
        return null;
    }
    @RequestMapping("/home")
    public String home() {
        return "deviceType";
    }
    /**
     * to device.jsp
     *
     * @return
     */
    @RequestMapping("/toDevice")
    public String toDevice(DeviceType deviceType, Map map) {
        List<DeviceType> deviceTypeList = deviceTypeService.countList();
        map.put("deviceTypeCountList", deviceTypeList);
        return "device";
    }

    /**
     *  to modeifydevice.jsp
     *
     * @return
     */
    @RequestMapping("/modify")
    public String modify(DeviceType deviceType, Map map) {
        List<DeviceType> deviceTypeList = deviceTypeService.countList();
        map.put("deviceTypeCountList", deviceTypeList);
        return "modifyDevice";

    }
}
