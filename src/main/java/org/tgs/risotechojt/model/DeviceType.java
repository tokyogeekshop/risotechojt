package org.tgs.risotechojt.model;

import java.io.Serializable;

/**
 * device_type
 */

public class DeviceType implements Serializable {
    private static final long serialVersionUID = 1L;
    /**key*/
    private Integer id;
    /**type name*/
    private String typeName;
    /**order number*/
    private Integer orderNo;
    /**in this type count*/
    private Integer deviceCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getDeviceCount() {
        return deviceCount;
    }

    public void setDeviceCount(Integer deviceCount) {
        deviceCount = deviceCount;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
