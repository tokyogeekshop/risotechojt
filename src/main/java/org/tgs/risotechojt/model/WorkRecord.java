package org.tgs.risotechojt.model;

import java.util.Date;

public class WorkRecord {

	private Long id;

	private String date;

	private Date workStartAt;

	private Date workEndAt;

	private String workDivision;

	private String workTime;

	private String completeFlag;

	private int sumtime;

	public int getSumtime() {
		return sumtime;
	}

	public void setSumtime(int sumtime) {
		this.sumtime = sumtime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Date getWorkStartAt() {
		return workStartAt;
	}

	public void setWorkStartAt(Date workStartAt) {
		this.workStartAt = workStartAt;
	}

	public Date getWorkEndAt() {
		return workEndAt;
	}

	public void setWorkEndAt(Date workEndAt) {
		this.workEndAt = workEndAt;
	}

	public String getWorkDivision() {
		return workDivision;
	}

	public void setWorkDivision(String workDivision) {
		this.workDivision = workDivision;
	}

	public String getWorkTime() {
		return workTime;
	}

	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}

	public String getCompleteFlag() {
		return completeFlag;
	}

	public void setCompleteFlag(String completeFlag) {
		this.completeFlag = completeFlag;
	}

}
