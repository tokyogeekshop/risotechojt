package org.tgs.risotechojt.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.tgs.risotechojt.model.Device;

public interface DeviceService {



	public List<Device> countList();

	public List<Device> list(Map<String,Object> map);

	public Long getTotal(Map<String,Object> map);

	public Device findById(Integer id);

	public Integer add(Device device);

	public Integer update(Device device);

	public Integer delete(Integer id);

}
