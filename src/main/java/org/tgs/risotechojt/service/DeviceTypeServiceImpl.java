package org.tgs.risotechojt.service;

import org.springframework.stereotype.Service;
import org.tgs.risotechojt.mappers.DeviceTypeMapper;
import org.tgs.risotechojt.model.DeviceType;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service("deviceTypeService")
public class DeviceTypeServiceImpl implements DeviceTypeService {
    @Resource
    private DeviceTypeMapper deviceTypeMapper;
    @Override
    public List<DeviceType> countList() {
        return deviceTypeMapper.countList();
    }
    @Override
    public DeviceType findById(Integer id) {
        return deviceTypeMapper.findById(id);
    }

    @Override
    public List<DeviceType> list(Map<String, Object> paramMap) {
        return deviceTypeMapper.list(paramMap);
    }

    @Override
    public Long getTotal(Map<String, Object> paramMap) {
        return deviceTypeMapper.getTotal(paramMap);
    }
    @Override
    public Integer add(DeviceType deviceType) {
        return deviceTypeMapper.add(deviceType);
    }
    @Override
    public Integer update(DeviceType deviceType) {
        return deviceTypeMapper.update(deviceType);
    }
    @Override
    public Integer delete(Integer id) {
        return deviceTypeMapper.delete(id);
    }
}
