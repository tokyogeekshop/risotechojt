package org.tgs.risotechojt.service;
import org.tgs.risotechojt.model.DeviceType;

import java.util.List;
import java.util.Map;

public interface DeviceTypeService {

    public List<DeviceType> countList();

    public DeviceType findById(Integer id);

    public List<DeviceType> list(Map<String,Object> paramMap);

    public Long getTotal(Map<String,Object> paramMap);

    public Integer add(DeviceType deviceType);

    public Integer update(DeviceType deviceType);

    public Integer delete(Integer id);
}
