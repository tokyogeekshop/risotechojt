package org.tgs.risotechojt.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tgs.risotechojt.mappers.WorkMapper;
import org.tgs.risotechojt.model.WorkRecord;

@Service("WorkService")
public class WorkServiceImpl implements WorkService {

	@Autowired
	private WorkMapper workMapper;

	public List<WorkRecord> getWorkTime(long id) {

		List<WorkRecord> work = workMapper.getlWorkMessage(id);

		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("1", "正常");
		hm.put("2", "全休");

		for (int i = 0; i < work.size(); i++) {

			String key = work.get(i).getWorkDivision();
			work.get(i).setWorkDivision(hm.get(key));
			Date b = work.get(i).getWorkStartAt();

			if (b != null) {
				Date t1 = work.get(i).getWorkEndAt();
				Date t2 = work.get(i).getWorkStartAt();
				long t3 = (t1.getTime() - t2.getTime()) / (1000 * 60 * 60);
				work.get(i).setWorkTime(Long.toString(t3));

			} else {

				work.get(i).setWorkTime(Long.toString(0));

			}

		}

		return work;
	}

	@Override
	public String Judgment(WorkRecord workRecord) {

		WorkRecord workmessage = workMapper.getWorkMessageByDateAndId(workRecord);

//判断完成状况，为1则返回over，再判断出勤时间，不为空返回f1，为空返回f2
		if ("1".equals(workmessage.getCompleteFlag())) {
			return "over";
		} else if (workmessage.getWorkStartAt() != null) {

			return "f1";
		} else {
			return "f2";
		}

	}

//更改出勤时间和出勤状况	
	public void updateStartTimeAndDivision(WorkRecord workRecord) {
		// 获取当日所有信息
		WorkRecord workRecord2 = workMapper.getWorkMessageByDateAndId(workRecord);
		workRecord2.setWorkStartAt(workRecord.getWorkStartAt());
		workRecord2.setWorkDivision("1");
		workMapper.UpdateWorkByDate(workRecord2);

	}

//更改退勤时间和完成状况
	public void updateEndTimeAndFlag(WorkRecord workRecord) {
		// 获取当日所有信息
		WorkRecord workRecord2 = workMapper.getWorkMessageByDateAndId(workRecord);
		workRecord2.setWorkEndAt(workRecord.getWorkEndAt());
		workRecord2.setCompleteFlag("1");
		workMapper.UpdateWorkByDate(workRecord2);

	}

//更改出勤状况和完成状况
	public void updateDivisionAndFlag(WorkRecord workRecord) {
		// 获取当日所有信息
		WorkRecord workRecord2 = workMapper.getWorkMessageByDateAndId(workRecord);
		workRecord2.setWorkDivision("2");
		workRecord2.setCompleteFlag("1");
		workMapper.UpdateWorkByDate(workRecord2);

	}

}