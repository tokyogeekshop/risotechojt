package org.tgs.risotechojt.service;

import java.util.List;
import org.tgs.risotechojt.model.WorkRecord;

public interface WorkService {

	List<WorkRecord> getWorkTime(long id);
	
//判断工时情况
	String Judgment(WorkRecord workRecord);

	void updateEndTimeAndFlag(WorkRecord workRecord);

	void updateDivisionAndFlag(WorkRecord workRecord);

	void updateStartTimeAndDivision(WorkRecord workRecord);

}
