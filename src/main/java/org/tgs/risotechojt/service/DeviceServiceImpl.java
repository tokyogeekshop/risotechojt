package org.tgs.risotechojt.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tgs.risotechojt.mappers.DeviceMapper;
import org.tgs.risotechojt.model.Device;
import org.tgs.risotechojt.service.DeviceService;
import javax.annotation.Resource;

@Service("deviceService")
public class DeviceServiceImpl implements DeviceService {
	@Resource
	private DeviceMapper deviceMapper;

	@Override
	public List<Device> countList() {
		return deviceMapper.countList();
	}

	@Override
	public List<Device> list(Map<String, Object> map) {
		return deviceMapper.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return deviceMapper.getTotal(map);
	}

	@Override
	public Device findById(Integer id) {
		return deviceMapper.findById(id);
	}

	@Override
	public Integer add(Device device) {
		return deviceMapper.add(device);
	}

	@Override
	public Integer update(Device device) {
		return deviceMapper.update(device);
	}

	@Override
	public Integer delete(Integer id) {
		return deviceMapper.delete(id);
	}
}
