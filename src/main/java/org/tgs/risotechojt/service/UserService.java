package org.tgs.risotechojt.service;

import java.util.List;
import org.tgs.risotechojt.model.User;


public interface UserService {

  void insertUser(User user);

  boolean getUserByLogin(String userName, String password);

  boolean getUserByUserName(String userName);

  void updateUser(User user);

  User getMesssage(long id);

  List<User> getalluser();

  List<User> userSearch(User user);


}
