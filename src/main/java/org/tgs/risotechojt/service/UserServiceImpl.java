package org.tgs.risotechojt.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tgs.risotechojt.mappers.UserMapper;
import org.tgs.risotechojt.model.User;


@Service("userService")
public class UserServiceImpl implements UserService {

  @Autowired
  private UserMapper userMapper;

  public void updateUser(User user) {
    userMapper.updateUser(user);
  }

  @Transactional
  public void insertUser(User user) {
    userMapper.insertUser(user);
  }

  public boolean getUserByLogin(String userName, String password) {
    User user = userMapper.getUserByUserName(userName);

    if (user != null && user.getPassword().equals(password)) {
      return true;
    }

    return false;
  }

  public boolean getUserByUserName(String userName) {
    User user = userMapper.getUserByUserName(userName);

    if (user != null) {
      return true;
    }

    return false;
  }

  public User getMesssage(long id) {
    User user = userMapper.getUserByUserId(id);
    return user;
  }

  public List<User> getalluser() {
    List<User> userall = userMapper.getAllUser();
    return userall;
  }

  public List<User> userSearch(User user) {
    List<User> usersearch = userMapper.searchUser(user);
    return usersearch;
  }

}
