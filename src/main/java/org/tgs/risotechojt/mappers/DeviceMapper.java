package org.tgs.risotechojt.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.tgs.risotechojt.model.Device;

public interface DeviceMapper {

	public List<Device> countList();

	public List<Device> list(Map<String,Object> map);

	public Long getTotal(Map<String,Object> map);

	public Device findById(Integer id);

	public Integer add(Device device);

	public Integer update(Device device);

	public Integer delete(Integer id);

}
