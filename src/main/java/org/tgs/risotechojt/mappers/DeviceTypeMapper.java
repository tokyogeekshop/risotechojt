package org.tgs.risotechojt.mappers;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.tgs.risotechojt.model.DeviceType;

import java.util.List;
import java.util.Map;

public interface DeviceTypeMapper {

    @Select(" select t2.id,t2.typeName,t2.orderNo,count(t1.id) as deviceCount from device t1 right join devicetype t2 on t1.type=t2.id group by t2.typeName order by t2.id")
    public List<DeviceType> countList();

    @Select("select * from devicetype where id=#{id}")
    public DeviceType findById(Integer id);

    @Select(" select * from devicetype order by orderNo limit #{start},${size}")
    public List<DeviceType> list(Map<String,Object> paramMap);

    @Select("select count(*) from devicetype")
    public Long getTotal(Map<String,Object> paramMap);

    @Insert("insert into devicetype(typeName,orderNo) values(#{typeName},#{orderNo})")
    public Integer add(DeviceType deviceType);

    @Update("update devicetype set typeName=#{typeName},orderNo=#{orderNo} where id=#{id}")
    public Integer update(DeviceType deviceType);

    @Delete("delete from devicetype where id=#{id}")
    public Integer delete(Integer id);
}
