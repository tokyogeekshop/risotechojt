package org.tgs.risotechojt.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.tgs.risotechojt.model.WorkRecord;

public interface WorkMapper {

	@Select("SELECT ID as id, DATE as date,WORKSTARTAT as workStartAt, WORKENDAT as workEndAt, "
			+ "WORKDIVISION as workDivision, COMPLETEFLAG as completeFlag "
			+ "FROM workRecord WHERE ID = #{id} and COMPLETEFLAG = 1")
	public List<WorkRecord> getlWorkMessage(long id);

	@Select("SELECT WORKSTARTAT as workStartAt, ID as id, DATE as date, WORKENDAT as workEndAt,"
			+ "WORKDIVISION as workDivision, COMPLETEFLAG as completeFlag " 
			+ "FROM workRecord WHERE date = #{date} "
			+ "and id = 44")
	public WorkRecord getWorkMessageByDateAndId(WorkRecord workRecord);

	@Update("UPDATE workRecord set workStartAt=#{workStartAt},workEndAt = #{workEndAt},"
			+ "completeFlag = #{completeFlag},workDivision = #{workDivision}" + "WHERE date= #{date} and id = 44")
	public void UpdateWorkByDate(WorkRecord workRecord);



}
