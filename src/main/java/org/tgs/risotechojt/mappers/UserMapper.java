package org.tgs.risotechojt.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.tgs.risotechojt.model.User;


public interface UserMapper {
  @Insert("INSERT INTO user(userName, password, firstName,"
      + "lastName, dateOfBirth, emailAddress,deviceId) VALUES"
      + "(#{userName},#{password}, #{firstName}, #{lastName},"
      + "#{dateOfBirth}, #{emailAddress},#{deviceId})")
  @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "id")
  public void insertUser(User user);

  @Select("SELECT USERNAME as userName, PASSWORD as password, "
      + "FIRSTNAME as firstName, LASTNAME as lastName, "
      + "DATEOFBIRTH as dateOfBirth, EMAILADDRESS as emailAddress,DEVICEID as deviceId "
      + "FROM user WHERE userName = #{userName}")
  public User getUserByUserName(String userName);

  @Select("SELECT ID as id, USERNAME as userName, PASSWORD as password, "
      + "FIRSTNAME as firstName, LASTNAME as lastName, "
      + "DATEOFBIRTH as dateOfBirth, EMAILADDRESS as emailAddress " + "FROM user WHERE id = #{id}")
  public User getUserByUserId(long id);

  @Update("UPDATE user set USERNAME=#{userName},PASSWORD=#{password},DATEOFBIRTH=#{dateOfBirth},EMAILADDRESS=#{emailAddress} "
      + "WHERE id=#{id}")
  public void updateUser(User user);

  @Select("SELECT ID as id, USERNAME as userName, PASSWORD as password, "
      + "FIRSTNAME as firstName, LASTNAME as lastName, "
      + "DATEOFBIRTH as dateOfBirth, EMAILADDRESS as emailAddress " + "FROM user ")
  public List<User> getAllUser();

  @Select("SELECT ID as id, USERNAME as userName, PASSWORD as password, "
      + "FIRSTNAME as firstName, LASTNAME as lastName, "
      + "DATEOFBIRTH as dateOfBirth, EMAILADDRESS as emailAddress "
      + "FROM user where USERNAME like \"%\"#{userName}\"%\"")
  public List<User> searchUser(User user);

}
